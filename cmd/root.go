/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package cmd

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/telemetry"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/tools"

	version_cmd "gitlab.com/sat-polsl/gcs/gcs-listener/cmd/version"
	"gitlab.com/sat-polsl/gcs/gcs-listener/version"
)

var (
	cfgFile string

	shutdownTracing func(context.Context) error
	shutdownMetrics func(context.Context) error
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   version.Name,
	Short: fmt.Sprintf("%s is a GCS Listener service", version.Name),
	Long: `FIXME: this app is product of building the app from a template repo
	
The repo is meant to simplify setting up new repos for CLI (and not only) apps.`,
	Version:       version.Version,
	RunE:          func(cmd *cobra.Command, args []string) error { return realMain(cmd.Context()) },
	SilenceErrors: true, // we handle errors ourselves
	SilenceUsage:  true, // we don't want to print usage on errors
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() error {
	ctx, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		cancel()
	}()

	if viper.GetBool("telemetry.enabled") {
		var err error

		host := viper.GetString("telemetry.otel_host")
		port := viper.GetString("telemetry.otel_port")

		tm := telemetry.New(
			telemetry.WithName(version.Name),
			telemetry.WithVersion(version.Version),
			telemetry.WithOTEL(host, port),
		)
		log.Trace().Caller().Str("otel_host", host).Str("otel_port", port).Msg("telemetry configured")

		shutdownMetrics, err = tm.ConfigureMetrics(ctx)
		if err != nil {
			log.Fatal().Err(err).Msg("failed to configure metrics")
		}

		shutdownTracing, err = tm.ConfigureTracing(ctx)
		if err != nil {
			log.Fatal().Err(err).Msg("failed to configure tracing")
		}
	}

	defer func() {
		if shutdownTracing != nil {
			deferCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			tools.WrapContext(deferCtx, shutdownTracing)
		}
	}()
	defer func() {
		if shutdownTracing != nil {
			deferCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			tools.WrapContext(deferCtx, shutdownMetrics)
		}
	}()

	return rootCmd.ExecuteContext(ctx)
}

func init() {
	initRootCmd()
	initSubCmd()
	cobra.OnInitialize(initConfig, initLogger)
	cobra.OnFinalize()
}

func initRootCmd() {
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default \"$HOME/.gcs.toml\")")

	rootCmd.PersistentFlags().String("log-level", "info", "configure default log level for application")
	_ = viper.BindPFlag("log_level", rootCmd.Flag("log-level"))

	rootCmd.PersistentFlags().String("grpc-port", "8080", "configure port for gRPC server")
	_ = viper.BindPFlag("grpc.port", rootCmd.Flag("grpc-port"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.SetDefault("log_level", "info")

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		if err != nil {
			log.Fatal().Err(err).Send()
		}

		// Search config in context directory
		viper.AddConfigPath(".")
		// Search config in home directory
		viper.AddConfigPath(home)
		viper.SetConfigType("toml")
		viper.SetConfigName(".gcs")
		viper.SetEnvKeyReplacer(strings.NewReplacer(`.`, `_`))
	}

	// NOTE: in CLI apps consider using viper.SetEnvPrefix("")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	_ = viper.ReadInConfig()

	viper.SetDefault("telemetry.otel_host", "otel-collector")
	viper.SetDefault("telemetry.otel_port", "4317")
	viper.SetDefault("telemetry.enabled", true)
}

func initLogger() {
	timeFormat := viper.GetString("log_timestamp_format")

	switch strings.ToUpper(timeFormat) {
	case zerolog.TimeFormatUnix, "UNIX":
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	case zerolog.TimeFormatUnixMicro:
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMicro

	case zerolog.TimeFormatUnixNano:
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnixNano

	case zerolog.TimeFormatUnixMs:
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMs

	case "RFC3339":
		zerolog.TimeFieldFormat = time.RFC3339

	case "RFC3339Nano":
		zerolog.TimeFieldFormat = time.RFC3339Nano

	case "RFC1123":
		zerolog.TimeFieldFormat = time.RFC1123

	case "RFC1123Z":
		zerolog.TimeFieldFormat = time.RFC1123Z

	case "RFC822":
		zerolog.TimeFieldFormat = time.RFC822

	case "RFC822Z":
		zerolog.TimeFieldFormat = time.RFC822Z

	case "RFC850":
		zerolog.TimeFieldFormat = time.RFC850

	default:
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	}

	lvl := viper.GetString("log_level")

	level, err := zerolog.ParseLevel(lvl)
	if err != nil {
		level = zerolog.InfoLevel
	}
	zerolog.SetGlobalLevel(level)
	log.Debug().Str("log_level", zerolog.GlobalLevel().String()).Send()

	if file := viper.ConfigFileUsed(); file != "" {
		log.Trace().Caller().Str("file", file).Msg("config file loaded")
	}
}

func initSubCmd() {
	rootCmd.AddCommand(version_cmd.NewVersionCmd())
}
