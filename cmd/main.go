/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"time"

	"github.com/go-zeromq/zmq4"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"google.golang.org/grpc"

	gcsgrpc "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/grpc"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/io"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/io/zmq"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/file"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/postgres"

	"gitlab.com/sat-polsl/gcs/gcs-listener/api"
)

func realMain(ctx context.Context) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	sub := zmq.NewSub(ctx, zmq4.NewSub(ctx))

	cs := api.NewConfigurationServer(
		api.WithOptionGetSetter(sub),
	)

	for _, topic := range viper.GetStringSlice("zeromq.topics") {
		if err := sub.SetOption(zmq4.OptionSubscribe, topic); err != nil {
			log.Error().Err(err).Msg("failed to subscribe to topic")
		}
		log.Trace().Caller().Str("topic", topic).Msg("subscribed to topic")
	}

	for _, topic := range viper.GetStringSlice("zeromq.beacon_topics") {
		if err := sub.SetOption(zmq4.OptionSubscribe, topic); err != nil {
			log.Error().Err(err).Msg("failed to subscribe to beacon topic")
		}
		log.Trace().Str("topic", topic).Msg("subscribed to beacon topic")
	}

	server := grpc.NewServer()
	gcsgrpc.RegisterConfiguratorServer(server, cs)

	go func() {
		log.Trace().Caller().Msg("starting gRPC server")
		err := listenAndServe(ctx, server)
		if err != nil {
			log.Error().Err(err).Msg("failed to listen and serve gRPC server")
		}
	}()

	client := io.NewClient(
		io.WithReceiver(sub),
	)

	s, err := createStorageClient(ctx, viper.GetString("storage.use"))
	if err != nil {
		return fmt.Errorf("storage client creation failed: %w", err)
	}
	defer timedClose(s)

	client.Use(logHandler())
	for _, topic := range viper.GetStringSlice("zeromq.beacon_topics") {
		client.HandleFunc(topic, storageHandler(s))
		log.Trace().Caller().Str("topic", topic).Msg("handling beacon topic with storageHandler")
	}

	host := viper.GetString("zeromq.proxy_address")
	port := viper.GetString("zeromq.sub_port")

	log.Trace().Caller().Msg("starting listener")
	err = client.ReceiveAndHandle(ctx, fmt.Sprintf("%s:%s", host, port))
	if err != nil {
		return fmt.Errorf("failed to receive and handle messages: %w", err)
	}

	return nil
}

func logHandler() func(ctx context.Context, msg io.Message) {
	return func(ctx context.Context, msg io.Message) {
		log.Trace().Caller().Str("topic", string(msg.Topic())).Bytes("data", msg.Data()).Send()
	}
}

func storageHandler(s storage.Storage) func(ctx context.Context, msg io.Message) {
	return func(ctx context.Context, msg io.Message) {
		var data map[string]interface{}
		err := json.Unmarshal(msg.Data(), &data)
		if err != nil {
			log.Error().Err(err).Msg("failed to unmarshal message data")
			return
		}

		err = s.Save(ctx, data)
		if err != nil {
			log.Error().Err(err).Msg("failed to save data")
			return
		}
	}
}

func createStorageClient(ctx context.Context, kind string) (storage.Storage, error) {
	switch kind {
	case "postgres":
		return postgres.New(ctx,
			postgres.WithCredentials(viper.GetString("storage.postgres.username"), viper.GetString("storage.postgres.password")),
			postgres.WithHost(viper.GetString("storage.postgres.host")),
			postgres.WithPort(viper.GetString("storage.postgres.port")),
			postgres.WithDatabase(viper.GetString("storage.postgres.database")),
		)
	case "file":
		return file.NewClient(ctx,
			file.WithFileType(viper.GetString("storage.file.format")),
			file.WithShardSize(viper.GetInt64("storage.file.shard_size")),
			file.WithContentDump(viper.GetBool("storage.file.dump_to_stdout")),
			file.WithStoragePath(viper.GetString("storage.file.output_dir")),
		), nil
	default:
		return nil, fmt.Errorf("unknown storage type: %s", kind)
	}
}

func listenAndServe(ctx context.Context, server *grpc.Server) error {
	lis, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", viper.GetInt("grpc.port")))
	if err != nil {
		return fmt.Errorf("failed to listen: %w", err)
	}

	go func() {
		err := server.Serve(lis)
		if err != nil {
			log.Error().Err(err).Msg("failed to serve")
		}
	}()

	// watch for context cancellation
	<-ctx.Done()
	server.GracefulStop()

	return nil
}

type closer interface {
	Close(context.Context) error
}

func timedClose(s closer) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	err := s.Close(ctx)
	if err != nil {
		log.Error().Err(err).Msg("failed to close storage client")
	}
	log.Trace().Caller().Msg("storage client closed")
}
