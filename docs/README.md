# Table of Contents

- [Deployment Instructions](docs/DEPLOYMENT.md)
- [Configuration Instructions](docs/CONFIGURATION.md)

---

| _Version_ | `v0.0.4` |
|-----------|------------------|
| _Date_    | `15 May 23 21:16 CEST`    |
