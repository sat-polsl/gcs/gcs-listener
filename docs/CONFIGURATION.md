# Configuration of the GCS Listener

## Configuration file

The configuration file is in TOML format. Application expects it to be placed either in `$HOME/.gcs.toml` or in the current context.

```toml
# `log_level` defines how verbose should the logging system be.
#
# Environment variable: `LOG_LEVEL`
#
# Possible values:
#   - "trace"
#   - "debug"
#   - "info" - default
#   - "warn"
#   - "error"
#   - "fatal"
#   - "panic"
#   - "disabled"
log_level = "info"

# `log_timestamp_format` defines format of timestamp attached to every message.
#
# Environment variable: `LOG_TIMESTAMP_FORMAT`
#
# Possible values:
#   - "UNIX" - default
#   - "UNIXMICRO"
#   - "UNIXNANO"
#   - "UNIXMS"
#   - "RFC3339"
#   - "RFC3339Nano"
#   - "RFC1123"
#   - "RFC1123Z"
#   - "RFC822"
#   - "RFC822Z"
#   - "RFC850"
log_timestamp_format = "UNIX"

# `[zeromq]` - section describing configuration of networking
[zeromq]

# `proxy_address` - specifies address of zmq proxy
#
# Environment variable: `ZEROMQ_PROXY_ADDRESS`
proxy_address = "localhost"

# `pub_port` - specifies port on zmq proxy, to which messages must be published
#
# Environment variable: `ZEROMQ_PUB_PORT`
pub_port = "14789"

# `sub_port` - specifies port on zmq proxy, from which messages must be read
#
# Environment variable: `ZEROMQ_SUB_PORT`
sub_port = "14790"

# `topics` - specifies topics to which app must be subscribed since the start
#
# Environment variable: `ZEROMQ_TOPICS`
topics = [
    "topic1",
    "topic2",
    "topic3",
]

# `beacon_topics` - specifies topics from which beacon messages must be handled
#
# Every beacon message must be a JSON object with the following structure:
#
# ```json
# {
#     "spacecraft_id": id,
#     ... // other fields
# }
# ```
#
# If the structure is not valid, the message will be ignored, and error will be logged.
#
# Environment variable: `ZEROMQ_BEACON_TOPIC`
beacon_topics = [
    "beacon"
]

# `[grpc]` - section describing configuration of gRPC server
[grpc]

# `port` - specifies port on which gRPC server should be listening
#
# Environment variable: `GRPC_PORT`
#
# Default value: `"8080"`
port = "8080"

# `[storage]` - section describing configuration of storage backend
[storage]

# `use` specifies which storage backend should be used
#
# Environment variable: `STORAGE_USE`
#
# Possible values:
#   - "file" - default
#   - "postgres"
use = "file"

    # `[storage.file]` - section describing configuration of file backend
    [storage.file]

    # `output_dir` - specifies the output directory where the data obtained from Proxy should be saved
    #
    # Environment variable: `STORAGE_FILE_OUTPUT_DIR`
    output_dir = "/path/to/storage/destination"

    # `format` defines format of the file
    #
    # Environment variable: `STORAGE_FILE_FORMAT`
    #
    # Possible values:
    #   - "json" - default
    #   - "txt"
    #   - "csv"
    format = "json"

    # `shard_size`
    #
    # Environment variable: `STORAGE_FILE_SHARD_SIZE`
    shard_size = "4MiB"

    # `dump_to_stdout`
    #
    # Environment variable: `STORAGE_FILE_DUMP_TO_STDOUT`
    dump_to_stdout = true

    # `[storage.postgres]` - section describing configuration of PostgreSQL backend
    [storage.postgres]

    # `host` is the host on which the PostgreSQL database is listening.
    #
    # Environment variable: `STORAGE_POSTGRES_HOST`
    host = "postgres"

    # `port` is the port on which the PostgreSQL database is listening
    #
    # Environment variable: `STORAGE_POSTGRES_PORT`
    port = 5432

    # `database` is the name of the PostgreSQL database
    #
    # Environment variable: `STORAGE_POSTGRES_DATABASE`
    database = "database"

    # `username` is the username for the PostgreSQL database
    #
    # Environment variable: `STORAGE_POSTGRES_USERNAME`
    username = "user"

    # `password` is the password for the user specified in `username` field for the PostgreSQL database
    #
    # Environment variable: `STORAGE_POSTGRES_PASSWORD`
    password = "Passw0rd+"

# `[telemetry]` - section describing configuration of telemetry
[telemetry]

# `enabled` sets whether telemetry should be enabled or not
#
# Environment variable: `TELEMETRY_ENABLED`
#
# Default value: `true`
enabled = true

# `otel_host` sets the name of the OpenTelemetry Collector service that will be used to send telemetry data. 
#
# Environment variable: `TELEMETRY_OTEL_SERVICE_NAME`
#
# Default value: `"otel-collector"`
otel_host = "otel-collector"

# `otel_port` sets the port of the OpenTelemetry Collector service that will be used to send telemetry data.
#
# Environment variable: `TELEMETRY_OTEL_SERVICE_PORT`
#
# Default value: `"4317"`
otel_port = "4317"

```

---

| _Version_ | `v0.0.4` |
|-----------|------------------|
| _Date_    | `15 May 23 21:16 CEST`    |
