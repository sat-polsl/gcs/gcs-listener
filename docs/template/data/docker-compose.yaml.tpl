services:
# ...

  # TODO: GCS Listener
  gcs-listener:
    image: registry.gitlab.com/sat-polsl/gcs/gcs-listener:{{ .Version }}
    restart: always
    environment:
      A: B

  # Jaeger
  jaeger-all-in-one:
    image: docker.io/jaegertracing/all-in-one:latest
    restart: always
    ports:
      - "16686:16686"
      - "14268"
      - "14250"

  # OpenTelemetry Collector
  otel-collector:
    image: docker.io/otel/opentelemetry-collector:0.67.0
    restart: always
    command: [ "--config=/etc/otel-collector-config.yaml" ]
    volumes:
      - ./otel-collector-config.yaml:/etc/otel-collector-config.yaml
    ports:
      - "1888:1888"   # pprof extension
      - "8888:8888"   # Prometheus metrics exposed by the collector
      - "8889:8889"   # Prometheus exporter metrics
      - "13133:13133" # health_check extension
      - "4317:4317"   # OTLP gRPC receiver
      - "55679:55679" # zpages extension
    depends_on:
      - jaeger-all-in-one
# ...