/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package api

import (
	"context"

	"github.com/rs/zerolog/log"
	gcsgrpc "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ConfigurationServer struct {
	configurator OptionGetSetter
	gcsgrpc.UnimplementedConfiguratorServer
}

type OptionGetSetter interface {
	GetOption(string) (interface{}, error)
	SetOption(string, interface{}) error
}

var _ gcsgrpc.ConfiguratorServer = (*ConfigurationServer)(nil)

type Option func(*ConfigurationServer)

func NewConfigurationServer(opts ...Option) *ConfigurationServer {
	s := &ConfigurationServer{}

	for _, opt := range opts {
		opt(s)
	}

	return s
}

func WithOptionGetSetter(gs OptionGetSetter) Option {
	return func(s *ConfigurationServer) {
		s.configurator = gs
	}
}

func (s *ConfigurationServer) Get(ctx context.Context, req *gcsgrpc.GetRequest) (*gcsgrpc.GetResponse, error) {
	// check if the option is not nil
	if req.Option == nil {
		log.Error().Msg("option is nil")
		return nil, status.Errorf(codes.InvalidArgument, "option is nil")
	}

	value, err := s.configurator.GetOption(req.Option.Name)
	if err != nil {
		log.Error().Err(err).Str("name", req.Option.Name).Msg("failed to get option")
		return nil, status.Errorf(codes.Internal, "failed to get option: %v", err)
	}

	v, ok := value.(string)
	if !ok {
		log.Error().Msg("failed to convert option value to string")
		return nil, status.Errorf(codes.Internal, "failed to convert option value to string")
	}

	log.Debug().Str("name", req.Option.Name).Str("value", v).Msg("option get")

	return &gcsgrpc.GetResponse{
		Option: &gcsgrpc.Option{
			Name:  req.Option.Name,
			Value: v,
		},
	}, nil
}

func (s *ConfigurationServer) Set(ctx context.Context, req *gcsgrpc.SetRequest) (*gcsgrpc.SetResponse, error) {
	if req.Option == nil {
		log.Error().Msg("option is nil")
		return nil, status.Errorf(codes.InvalidArgument, "option is nil")
	}

	if err := s.configurator.SetOption(req.Option.Name, req.Option.Value); err != nil {
		log.Error().Err(err).Str("name", req.Option.Name).Str("value", req.Option.Value).Msg("failed to set option")
		return nil, status.Errorf(codes.Internal, "failed to set option: %v", err)
	}

	log.Debug().Str("name", req.Option.Name).Str("value", req.Option.Value).Msg("option set")

	return &gcsgrpc.SetResponse{
		Option: &gcsgrpc.Option{
			Name:  req.Option.Name,
			Value: req.Option.Value,
		},
	}, nil
}
